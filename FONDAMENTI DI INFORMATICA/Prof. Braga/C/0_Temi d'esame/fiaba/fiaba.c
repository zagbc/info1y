#include <stdio.h>
#include <string.h>

#define max 200
#define dimp 20

typedef struct
{
    char parola[dimp+1];
    int occorrenza;

}t_parola;

//void occorrenza (t_parola p1);
int pif (char * frase, char * par);

int main(int argc, char* argv[])
{

    FILE* fp;
    t_parola par1;
    char riga [max];
    char temp_par [dimp+1];
    int cnt = 0;

    fp = fopen(argv[1],"r");

    if (fp == NULL)
    {
        printf("ERRORE APERTURA FILE\n");
        return 1;
    }

    while(fscanf(fp,"%s",temp_par) != EOF)
    {
        printf("\nTEMP PAR: %s\nRIGA: %s\n",temp_par,riga);
        cnt = pif(temp_par,fgets(riga,max,fp));

    }

    printf("\n%d\n",cnt);

    return 0;

}

int pif (char * frase, char * par)
{
	
	int j = 0, i = 0;
	char temp [max];
    int cnt = 0;
	
	do
	{
	
		for(i = j; (frase[i] >= 'a' && frase[i] <= 'z'); i++)
		{
			temp[i-j] = frase[i];
			
		}
		
		temp[i] = '\0';
		
		//printf("TEMP: %s\nPAR: %s\ni: %d\nj: %d\n",temp,par,i,j);
		
		if (strcmp(par,temp) == 0)
		{
			cnt++;;	
		}
		
        j=i+1;
	
	}while(frase[j-1] != '\0');
	
	return cnt;
	
}