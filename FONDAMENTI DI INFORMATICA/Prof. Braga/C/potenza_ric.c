#include <stdio.h>

int pot_ric(int b, int e);

int main (void)
{

    int base,expo;

    printf("Inserisci base: ");
    scanf("%d", &base);

    printf("Inserisci esponente: ");
    scanf("%d", &expo);

    printf("%d\n", pot_ric(base,expo));

}

int pot_ric(int b, int e)
{

    if(e == 0)
        return 1;
    else
        return b*pot_ric(b,(e-1));

}