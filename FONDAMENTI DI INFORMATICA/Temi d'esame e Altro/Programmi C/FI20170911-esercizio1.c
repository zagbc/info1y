/*
Scrivere una funzione C che riceve come parametro un numero intero n. Il sottoprogramma calcola e restituisce le coppie di interi positivi la cui somma è pari a n.

Note: utilizzare la struttura dati più adatta per restituire le coppie di interi positivi a somma n; l’ordine degli interi restituiti in ciascuna coppia è
irrilevante: se viene restituita la coppia (x,y) non occorre restituire anche la coppia (y,x).

Esempi
ingresso: 8; uscita: (1,7) (2,6) (3,5) (4,4)
ingresso: 11; uscita: (1,10) (2,9) (3,8) (4,7) (5,6)
*/

typedef struct {
    int x;
    int y;
} coppiaInteri;

coppiaInteri* funzione(int n) {
    coppiaInteri *c = malloc(sizeof(coppiaInteri)*n/2);

    for(int i=0; i<n/2; i++) {
        c[i].x = i+1;
        c[i].y = n-i-1;
    }
    return c;
}
